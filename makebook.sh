#!/usr/bin/env bash  

# clear out the file
> book.html
for dir in */ ; do
	for hf in ${dir}*.html
	do
		echo "\n <!-- ===================  SECTION: $hf  =================== --> \n" >> book.html
		sed -n '/<h1/,$p' $hf >> book.html
	done
done
